// navebar section
$(document).ready(function(){
  "use strict";

  var nav_offset_top = $('header').height() + 100;

  //* Navbar Fixed
  function navbarFixed(){
      if ( $('header').length ){
          $(window).scroll(function() {
              var scroll = $(window).scrollTop();
              if (scroll >= nav_offset_top) {
                  $("header").addClass("navbar_fixed");
              } else {
                  $("header").removeClass("navbar_fixed");
              }
          })
      }
  }
  navbarFixed();
});

// end navebar section



// sileder js
$('#myCarousel').carousel({
   interval: 3000,
})
// end slide
